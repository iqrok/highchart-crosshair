$(function () {
	let crosshair = null;
	let rect = null;
	let image = null;

	const chart = new Highcharts.Chart({

		chart: {
			renderTo: 'container',
			type: 'area',
		},
		plotOptions: {

			area: {
				marker: {
					enabled: false,
				}
			},
		},
		series: [{
				data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 1000, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
			}]

	});

	// x and y calculated using Highcharts function
	function drawCrosshair(X, Y, crosshairSize = 10, zIndex = 2000) {
		const xAxis = chart.xAxis[0];
		const yAxis = chart.yAxis[0];

		const x = xAxis.toPixels(X);
		const y = yAxis.toPixels(Y);

		// if crosshairSize is 0 or falsy, draw croshair full length
		const coordinates = crosshairSize
			? [
				// draw horizontal line at y coordinate
				'M', (x - crosshairSize), y,
				'L', (x + crosshairSize), y,

				// draw vertical line at x coordinate
				'M', x, (y - crosshairSize),
				'L', x, (y + crosshairSize),
			]
			: [
				'M', chart.plotLeft, y,
				'L', chart.plotLeft + chart.plotWidth, y,

				'M', x, chart.plotTop,
				'L', x, chart.plotTop + chart.plotHeight,
			];

		if (crosshair){
			crosshair.destroy();
		}

		crosshair = chart.renderer
			.path(coordinates)
			.attr({
				'stroke-width': 2,
				stroke: '#FF0000',
				zIndex,
			})
			.add();
	}

	// x and y calculated using Highcharts function
	function drawImage(src, X, Y, size = 10, zIndex = 3000) {
		const xAxis = chart.xAxis[0];
		const yAxis = chart.yAxis[0];

		const x = xAxis.toPixels(X);
		const y = yAxis.toPixels(Y);

		if (image){
			image.destroy();
		}

		image = chart.renderer
			.image(src, x-(size/2), y-(size/2), size, size)
			.attr({
				zIndex,
			})
			.add();
	}

	// x and y calculated manually using chart properties
	function drawCrosshair2(X, Y, crosshairSize = 10, zIndex = 2000) {
		// find x coordinates
		const xWidth = chart.xAxis[0].width;
		const xOldWidth = chart.xAxis[0].oldAxisLength;
		const xPad = chart.xAxis[0].transA;
		const xOffset = chart.series[0].points[0].plotX;
		const x = chart.plotLeft + (X * xPad) + xOffset;

		// find y coordinates
		const yHeight = chart.yAxis[0].height;
		const yOldheight = chart.yAxis[0].oldAxisLength;
		const yPad = (yHeight/chart.yAxis[0].max);
		const yOffset = (yOldheight/chart.yAxis[0].max);
		const y = chart.plotTop + yHeight - ((Y * yPad) + yOffset);

		if (crosshair){
			crosshair.destroy();
		}

		// if crosshairSize is 0 or falsy, draw croshair full length
		const coordinates = crosshairSize
			? [
				'M', (x - crosshairSize), y,
				'L', (x + crosshairSize), y,

				'M', x, (y - crosshairSize),
				'L', x, (y + crosshairSize),
			]
			: [
				'M', chart.plotLeft, y,
				'L', chart.plotLeft + chart.plotWidth, y,

				'M', x, chart.plotTop,
				'L', x, chart.plotTop + chart.plotHeight,
			];

		//Use above coordinates to draw line
		crosshair = chart.renderer
			.path(coordinates)
			.attr({
				'stroke-width': 2,
				stroke: '#0000FF',
				zIndex,
			})
			.add();

	}

	function drawRectangle(X1, Y1, X2, Y2){
		const xAxis = chart.xAxis[0];
		const yAxis = chart.yAxis[0];

		if (rect){
			rect.destroy();
		}

		rect = chart.renderer.rect(
				xAxis.toPixels(X1),
				yAxis.toPixels(Y1),
				xAxis.toPixels(X2) - xAxis.toPixels(X1),
				Math.abs(yAxis.toPixels(Y2) - yAxis.toPixels(Y1)),
			)
			.attr({
				'stroke-width': 2,
				stroke: '#00FF80',
				fill: 'rgba(0,255,128,0.25)',
				zIndex: 1000
			})
			.addClass('rect')
			.add();

	}

	// assign value for rectangle and calculated each point to get correct points to draw rectangle
	const rectangle = {
			point1 : {
				x : Math.random() * 23,
				y : Math.random() * 1000,
			},

			point2 : {
				x : Math.random() * 23,
				y : Math.random() * 1000,
			},

			get rect() {
				return {
					x1 : this.point1.x < this.point2.x ? this.point1.x : this.point2.x,
					x2 : this.point1.x > this.point2.x ? this.point1.x : this.point2.x,
					y1 : this.point1.y > this.point2.y ? this.point1.y : this.point2.y,
					y2 : this.point1.y < this.point2.y ? this.point1.y : this.point2.y,
				};
			},

			get limit() {
				return {
					x : Math.abs(this.point1.x - this.point2.x),
					y : Math.abs(this.point1.y - this.point2.y),
				};
			},
		};

	drawRectangle(rectangle.rect.x1, rectangle.rect.y1, rectangle.rect.x2, rectangle.rect.y2);

	setInterval(()=>{
			// make sure crosshair is inside rectangle
			const _x = rectangle.rect.x1 + Math.random() * rectangle.limit.x;
			const _y = rectangle.rect.y2 + Math.random() * rectangle.limit.y;

			drawCrosshair(_x, _y, 20, 2200);
			drawImage('./crosshair.svg',_x, _y, 35, 2100);
			//~ drawImage('./crosshair.svg', 12, 1000, 30);
			//~ drawCrosshair(12, 1000, 0);
		},2150)
});
